#initial_time 1.985 sec
#after index 1.654 sec
#after PK 0.873 sec 
use uniport_example;

select * from proteins;
load data local infile 'C:\\Users\\Mennah\\Desktop\\insert.txt' into table proteins fields terminated by '|';

select *
from proteins 
where protein_name like "%tumor%" and uniport_id like "%human%"
order by uniport_id;

create index uniport_index on proteins (uniport_id);
drop index uniport_index on protein;

alter table proteins add constraint pk_proteins primary key (uniport_id);