# 6. Show the thriller films whose budget is greater than 25 million$.	 
select title
from movies join genres on movies.movie_id = genres.movie_id
where budget > 25000000 and genre_name = "Thriller";


# 7. Show the drama films whose language is Italian and produced between 1990-2000.
select title
from movies, languages, genres
where year between 1990 and 2000 and language_name="Italian" and genre_name="Drama" and movies.movie_id=languages.movie_id=genres.movie_id;


select title
from movies
where year between 1990 and 2000 and movie_id in(
	select genres.movie_id
    from languages join genres on languages.movie_id= genres.movie_id
    where language_name ="Italian" and genre_name="Drama");
    
    
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.
select title
from movies 
where oscars > 3 and movie_id in (
	select movie_id
    from stars join movie_stars on stars.star_id=movie_stars.star_id
    where star_name = "Tom Hanks");
    

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select title
from movies
where duration between 100 and 200 and movie_id in(
	select movie_id
    from producer_countries join countries on producer_countries.country_id=countries.country_id
    where country_name="USA"and movie_id in(
		select movie_id
		from genres
		where genre_name = "History")
    );


# 10.Compute the average budget of the films directed by Peter Jackson.
select avg(budget)
from movies
where movie_id in (
	select movie_id
    from directors join movie_directors on directors.director_id=movie_directors.director_id
    where director_name="Peter Jackson");


# 11.Show the Francis Ford Coppola film that has the minimum budget.
select title, budget
from movies
where movie_id in (
	select movie_id
    from directors join movie_directors on directors.director_id=movie_directors.director_id
    where director_name="Francis Ford Coppola")
order by budget asc # default olan bu, desc buyukten kucuge siralar
limit 1;


# 12.Show the film that has the most vote and has been produced in USA.
select title, votes
from movies
where movie_id in(
	select movie_id
    from producer_countries join countries on producer_countries.country_id=countries.country_id
    where country_name="USA")
order by votes desc
limit 1;
