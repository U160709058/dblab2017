use movie_db;

create view title_director as
select title,director_name
from movies,directors,movie_directors
where movies.movie_id = movie_directors.movie_id and 
directors.director_id = movie_directors.director_id;

select * from title_director;

create view action_movies as 
select movies.movie_id,movies.title
from movies join genres on movies.movie_id = genres.movie_id
where genre_name like "%action%";

call GetMovies();
CALL `movie_db`.`GetMovieByDirector`("Peter Jackson");
call GetMovieByCoStar("Ian McKellen","Orlando Bloom");
